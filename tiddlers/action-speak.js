/*\
title: $:/plugins/OokTech/RatReader/action-speak.js
type: application/javascript
module-type: widget

Action widget to parse an epub for the rat reader

\*/
(function(){

/*jslint node: true, browser: true */
/*global $tw: false */
"use strict";

var Widget = require("$:/core/modules/widgets/widget.js").widget;

var SpeakWidget = function(parseTreeNode,options) {
	this.initialise(parseTreeNode,options);
};

/*
Inherit from the base widget class
*/
SpeakWidget.prototype = new Widget();

/*
Render this widget into the DOM
*/
SpeakWidget.prototype.render = function(parent,nextSibling) {
	this.synth = window.speechSynthesis;
	this.utterance = new window.SpeechSynthesisUtterance("");
	this.utterance.onboundary = function(event) {
		// make sure it is actually playing, if so move to next word
		const tid2 = $tw.wiki.getTiddler('$:/state/OokTech/RatReader');
		if(tid2 && tid2.fields.play === 'yes') {
			const tiddler = $tw.wiki.getTiddler('$:/plugins/OokTech/RatReader/NextButtonActions');
			if(tiddler) {
				$tw.rootWidget.invokeActionString(tiddler.fields.text,$tw.rootWidget);
			}
		} else {
			if(this.synth) {
				this.synth.pause();
			}
		}
	}
	this.computeAttributes();
	this.execute();
};

/*
Compute the internal state of the widget
*/
SpeakWidget.prototype.execute = function() {
	this.text = this.getAttribute("text");
};

/*
Refresh the widget by ensuring our attributes are up to date
*/
SpeakWidget.prototype.refresh = function(changedTiddlers) {
	const changedAttributes = this.computeAttributes();
	if(Object.keys(changedAttributes).length) {
		this.refreshSelf();
		return true;
	}
	return this.refreshChildren(changedTiddlers);
};

/*
Invoke the action associated with this widget
*/
SpeakWidget.prototype.invokeAction = function(triggeringWidget,event) {
	if(this.synth.speaking) {
		this.synth.cancel();
	}
	if(this.text) {
		this.utterance.text = this.text;
		this.synth.speak(this.utterance);
	}

	return true; // Action was invoked
};

exports["action-speak"] = SpeakWidget;

})();
