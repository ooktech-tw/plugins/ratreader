title: $:/plugins/OokTech/RatReader/readme

Note: this is new, there are certainly bugs so treat it like a demo, not something finished. It should be usable, but don't be surprised when something doesn't work. The menu is also not very polished, the layout of the reader part itself is probably not going to change much, but the menu should be cleaned up so it is easier to use eventually.

If you just want to get started and learn by experimenting, you can <$button>Start Reader Mode<$action-setfield $tiddler='$:/StoryList' list='$:/plugins/OokTech/RatReader/Reader'/><$action-setfield $tiddler='$:/plugins/OokTech/RatReader/settings' reader_mode=yes/><$action-setfield $tiddler='$:/state/sidebar' text=no/></$button>

!Rat Reader

An RSVP reader in TiddlyWiki.

[[What is RSVP?|https://en.wikipedia.org/wiki/Rapid_serial_visual_presentation]]

!!What is this?

A tool for reading. You can either paste text in and use it that way, or you can use it to parse an epub file and then read it.

When reading only three words are displayed on the screen at a
time and you read the center word then the words all shift to the left and you
read the next word.

People often refer to readers like this as speed readers, but in my experience
the biggest benefit is comfort when reading on a small screen, not speed.

!!Features

* RSVP reader
** Adjustable text speed
* Bulk text reader
* Text to speech
* Save reading position when switching between all reading modes
* Load epubs from a url
* Load epubs from a tiddler (drop an epub file in the wiki)
* view text manually entered or pasted into the wiki
* bookmark places in each text
* save current place between wiki loads
* navigate forward and backward though the text
** word by word
** paragraph by paragraph
** chapter by chapter
* adjust font size and colors
** Either use the wiki pallet or manually select colors
* Reader mode that removes everything but the reader UI from sight
* Click on the book name to open a list of bookmarks, chapters and available books

!!Why?

Because I read a lot and am on the metro a lot. I find it difficult to read an
ebook normally on a phone but I don't have trouble when using a reader like
this.

!!How?

The plugin is almost entirely made with wikitext. There are only two javascript
components:

* A custom timer. it runs the actions of the next word button to match the selecetd words per minute. It is close to the minimum amount of code I could use and it triggers the same wikitext action widgets as the next word button.
* ePub.js to parse epub files. It isn't needed for the reader part, but the reader isn't very interesting if you can't load books.
* `$action-parseepub` is a widget that uses ePub.js to load an epub file and parse it so it is in the correct format for the reader.

!!How do I use it?

The reader itself is in [[$:/plugins/OokTech/RatReader/Reader]].

# First, you must select what you want to read. <div>

Click on the rat face to open the menu.

<h3>- Entering text manually - no parsing</h3>

If you want to paste in text from somewhere else (of type something) select `Paste Text` from the select widget at the top of the menu.
Paste or enter the text into the text box that appears.
If it is relatively short (more than a few pages) you can use it directly, uncheck the `Use Parsed Text` checkbox. menu.

<h3>- Selecting an existing book</h3>

In the select menu pick an existing book.

<h3>- Adding a new book by url</h3>

Note: there is some weirdness with adding new epubs from remote servers. It doesn't work very consistently.

Click the `Add New Book` button and enter the url to the epub file you want to add.
Then select that book from the select menu.

<h3>- Adding a new book already in the wiki</h3>

This isn't implemented yet.

</div>
# You can then adjust the reader settings. the most used settings are:
#* `Enter reader mode` check this box to enter reader mode. It adjusts the layout to only show the reader itself and removes everything else. Uncheck the box to exit reader mode.
#* RSVP text font size is how big the font in the reader is in pixels.
#* RSVP Text colors
#** if you check `Use Pallet Colors` the text colors are taken from the currently selected pallet for the wiki. (Set the pallet in the $:/ControlPanel -> Apperance -> pallet tab.)
#** If you uncheck `Use Pallet Colors` you can manually set the text colors in the RSVP reader by clicking on the colored rectangle. The `Primary` color is the color for the center word, the `Middle` color is the color of the highlighted letter in the middle word, the `Secondary` color is the color of the words that are shown before and after the middle word.
#* Checking `Show Bulk Text` shows the text in paragraph form. Not all of the text is displayed, just the current paragraph and a few of the preceding and following paragraphs. You can adjust how many paragrahs are shown by changing the Show pargaphs before and after values.
#** The biggest use of the Bulk Text mode is to select a spot in the text. The current word for the rsvp reader is shown with a colored background, clicking on another word in the current paragraph. Clicking on another paragraph sets that as the current paragraph. Using this you can more quickly pick a spot in the text if you lose your place.
#* (text to speech isn't implemented yet)
#* `Progress Bar Visibility` There are number of different progress bars that you can disply above the RSVP text. Checking the box next to one makes it visible.
#** `Book % Progress` shows the percentage of the entire book that you have finished
#** Chapters progress shows your progress through the book counted in chapters (current chapter out of the total number of chapters in the book)
#** Paragraph progress shows your progress through the current chapter in paragraphs (current paragraph out of the total number of paragraphs in the chapter)
#** Word progress shows your progress in the current paragraph in words (current word compared to the total number of words in the paragraph)
# Close the menu by clicking on the rat face
# The UI has a few parts:
#* The book title is shown in the middle of the top
#* The reading time left at the current WPM (words per minute) is below the title
#* Under that is the text (RSVP or bulk text)
#* Below that are the navigation buttons. The three arrow buttons on the outsides move forward or backward by one chapter, the two arrow buttons move forward or backward by one paragarph and the single arrow buttons move forward or backward by one word. In the center is the play/pause button which starts or stops the text.
#* At the bottom is a range slider that lets you set the words per minute, below the slider is a text box that shows the current WPM and lets you manually set the value if you want.
#* Clicking on the rat face opens and closes the menu.
#* Clicking on the next/previous word moves forward or backward by one word
# Click the play button to start reading, adjust the WPM to a comfortable speed and enjoy. If you have trouble the first thing to adjust is the WPM, then the text colors (generally make the center word very visible, the middle letter have a high contrast with the center word and the other two words low contrast with the background) and then the text size.


-----

!The `$action-parseepub` widget

This plugin contains a widget that loads an epub file from a url and parses it so it can be used with the reader.

The Widget will take the following inputs

|!Attribute |!Description |
|url |The url to the epub file. |


!Example:

```
<$button
>
  Get some epub
  <$action-parseepub
    url='some/url/here.epub'
  />
</$button>
```

! Future Plans

* options to set the fonts used
* (possibly) options to change UI colors independent of the wiki color pallet
** The text colors are changable, not other things
* Click on the center word to start/stop
* enable clicking and drag/swipe to move forward and backward in the text
* enable scrubbing though the text using the progress bars
* more options for the bulk text display
* since epub.js is there anyway, add a view that is a normal book view of the epubs that are available.

[[The ePub.js License|$:/plugins/OokTech/RatReader/ePub.js License]]
