/*\
title: $:/plugins/OokTech/RatReader/RatTimer.js
type: application/javascript
module-type: startup

This is the daemon process that runs a timer and periodically checks to see if
there is a new task to run based on the ellapsed time.

\*/
(function () {

  /*jslint node: true, browser: true */
  /*global $tw: false */
  "use strict";

  // Export name and synchronous status
  exports.name = "reader-daemon";
  exports.platforms = ["browser"];
  exports.after = ["render"];
  exports.synchronous = true;

  exports.startup = function() {
    var timerID
    var intervalDelay=1000;
    var wait = true;
    if(window.speechSynthesis && window.SpeechSynthesisUtterance) {
      var synth = window.speechSynthesis;
      var utterance = new window.SpeechSynthesisUtterance("");
      utterance.onboundary = function(event) {
    		// make sure it is actually playing, if so move to next word
    		const tid2 = $tw.wiki.getTiddler('$:/state/OokTech/RatReader');
    		if(tid2 && tid2.fields.play === 'yes') {
    			const tiddler = $tw.wiki.getTiddler('$:/plugins/OokTech/RatReader/NextButtonActions');
    			if(tiddler && !wait) {
    				$tw.rootWidget.invokeActionString(tiddler.fields.text,$tw.rootWidget);
    			}
    		} else {
    			if(synth) {
    				synth.cancel();
    			}
    		}
        wait = false;
    	}
      utterance.onend = function(event) {
    		// make sure it is actually playing, if so move to next word
    		const tid2 = $tw.wiki.getTiddler('$:/state/OokTech/RatReader');
    		if(tid2 && tid2.fields.play === 'yes') {
    			const tiddler = $tw.wiki.getTiddler('$:/plugins/OokTech/RatReader/NextButtonActions');
    			if(tiddler) {
    				$tw.rootWidget.invokeActionString(tiddler.fields.text,$tw.rootWidget);
            $tw.wiki.setText('$:/state/OokTech/RatReader', 'current_index', 0);
            wait = true;
    			}
    		} else {
    			if(synth) {
    				synth.cancel();
    			}
    		}
    	}
    }

    function checkStatus() {
      var tid1 = $tw.wiki.getTiddler('$:/plugins/OokTech/RatReader/settings');
      var tid2 = $tw.wiki.getTiddler('$:/state/OokTech/RatReader');
      if(tid1 && tid2 && tid2.fields.play === 'yes' && tid1.fields.use_speech !== 'yes') {
        var newDelay = (tid1.fields.wpm && tid1.fields.wpm > 0) ? Math.floor(60000 / tid1.fields.wpm) : 100;
        if (Number(newDelay) !== Number(intervalDelay)) {
          intervalDelay = newDelay;
          clearInterval(timerID);
          timerID = setInterval(checkStatus, intervalDelay);
        }
        var tiddler = $tw.wiki.getTiddler('$:/plugins/OokTech/RatReader/NextButtonActions');
        if(tiddler) {
          $tw.rootWidget.invokeActionString(tiddler.fields.text,$tw.rootWidget);
        }
      } else if(tid1 && tid2 && tid2.fields.play === 'yes' && tid1.fields.use_speech === 'yes' && !synth.speaking) {
        // make it speak!
        const currPara = tid2.fields.current_paragraph;
        const currInd = tid2.fields.current_index > 3 ? tid2.fields.current_index : 1;
        const tid3 = $tw.wiki.getTiddlerData('$:/state/OokTech/RatReader/ParsedText');
        if(tid3) {
          $tw.wiki.setText('$:/state/OokTech/RatReader', 'current_index', undefined, currInd);
          utterance.text = tid3[currPara].split(' ').slice(currInd - 1).join(' '); // the text for this paragraph starting at the current index
          synth.speak(utterance);
        }
      }
    }
    intervalDelay = 1000;
    if (intervalDelay) {
      timerID = setInterval(checkStatus, intervalDelay);
    }
  }

})();
