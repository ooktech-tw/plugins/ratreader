/*\
title: $:/plugins/OokTech/RatReader/action-parseepub.js
type: application/javascript
module-type: widget

Action widget to parse an epub for the rat reader

\*/
(function(){

/*jslint node: true, browser: true */
/*global $tw: false */
"use strict";

var Widget = require("$:/core/modules/widgets/widget.js").widget;

var ParseEPUBWidget = function(parseTreeNode,options) {
	this.initialise(parseTreeNode,options);
};

/*
Inherit from the base widget class
*/
ParseEPUBWidget.prototype = new Widget();

/*
Render this widget into the DOM
*/
ParseEPUBWidget.prototype.render = function(parent,nextSibling) {
	this.computeAttributes();
	this.execute();
};

/*
Compute the internal state of the widget
*/
ParseEPUBWidget.prototype.execute = function() {
	this.url = this.getAttribute("url");
  this.name = this.getAttribute("name", "")
};

/*
Refresh the widget by ensuring our attributes are up to date
*/
ParseEPUBWidget.prototype.refresh = function(changedTiddlers) {
	const changedAttributes = this.computeAttributes();
	if(Object.keys(changedAttributes).length) {
		this.refreshSelf();
		return true;
	}
	return this.refreshChildren(changedTiddlers);
};

/*
Invoke the action associated with this widget
*/
ParseEPUBWidget.prototype.invokeAction = function(triggeringWidget,event) {
	if(this.url) {
		console.log(`load from url ${this.url}`)
  	var book = new ePub.Book(this.url,{encoding:'binary'})
	} else {
		console.log(`load from tiddler ${this.name}`)
		const thisText = $tw.wiki.getTiddlerText(this.name)
		var book = new ePub.Book(thisText, {encoding:'base64'})
	}

  book.loaded.navigation.then(function(blah) {
			book.opened.then(function(thisOpened) {
			  const container = thisOpened.container
				const thePackagePath = container.packagePath.startsWith('/') ? container.packagePath : '/' + container.packagePath
				return book.load(thePackagePath)
			}).then(function(containerInfo){
				const theInfo = book.package.parse(containerInfo)
				const author = theInfo.metadata.creator
				const title = theInfo.metadata.title

				const creationFields = $tw.wiki.getCreationFields();
	      const fields1 = {
	        title: `${title} info`,
	        num_chapters: book.spine.items.length,
					book_name: title,
					author: author,
					tags: 'RatReaderBook'
	      }
				$tw.wiki.addTiddler(new $tw.Tiddler(creationFields,fields1));
	      for(let i = 0; i < book.spine.items.length; i++) {
					const tmpURL = book.spine.items[i].url
	        book.load(tmpURL)
	          .then(function(theHtml) {
	            const creationFields = $tw.wiki.getCreationFields();
	            const fields = {
	              title: `${title} - chapter ${i}`,
	              text: theHtml.children[0].innerText,
								book_name: title,
								chapter: i,
								num_words: theHtml.children[0].innerText.split(" ").filter(function(item){return item.length > 0}).length
	            }
	            $tw.wiki.addTiddler(new $tw.Tiddler(creationFields,fields));

	          })
	      }
	    })
		})

	return true; // Action was invoked
};

exports["action-parseepub"] = ParseEPUBWidget;

})();
